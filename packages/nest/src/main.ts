import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { helloWorld } from "@ui/common/src";

const bootstrap = async () => {
  console.log("@@@@@@@@@@@@@@@@@");
  helloWorld();
  const app = await NestFactory.create(AppModule);
  await app.listen(4000);
};

bootstrap();
